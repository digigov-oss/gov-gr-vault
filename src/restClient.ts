import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import {
    Attachment,
    GateRetrieveDocumentsRequest,
    GateCreatePostDocumentRequest,
    GateCreatePostDocumentResponse,
    Statement,
    Target,
    GateRetrieveDocumentsResponse,
} from './interface';

import axios from 'axios';

export class RestClient {
    user: string;
    pass: string;
    endpoint: string;

    constructor(user: string, pass: string, endpoint: string) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }

    /**
     *
     * @param path string;
     * @returns
     */
    gateCreatePostDocument = async (
        gate: string,
        template: string,
        statements: Statement,
        attachments: Attachment,
        targets: Target[],
        auditRecord: AuditRecord,
        actor: string = '',
        isOfficial: boolean = false,
        export_document_pdf: boolean = false,
    ): Promise<GateCreatePostDocumentResponse> => {
        const request: GateCreatePostDocumentRequest = {
            auditRecord: auditRecord,
            gateCreatePostDocumentInputRecord: {
                gate: gate,
                source: {
                    document: {
                        statements: statements,
                        attachments: attachments,
                        issuer: 'services.gov.gr', //always issuer is services.gov.gr
                        is_official: isOfficial, //βάζει σήμανση ΑΚΥΡΟ στο document εάν είναι test
                        template: {
                            refname: template,
                            'digest-sha256': null, //This does not used and is always null
                        },
                        timestamp: null, //This does not used and is always null
                        'document-id': null, //This does not used and is always null
                        state: 'issued',
                        'digest-sha256': '', //This does not used and is always empty string
                    },
                    targets: targets,
                    export_document_pdf: export_document_pdf
                },
            },
        };
        if (actor != '') {
            request.gateCreatePostDocumentInputRecord.source.actor = {
                id: actor,
            };
        }
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        // post the request to the endpoint
        const ed = this.endpoint + '/gateCreatePostDocument';
        const response = await axios.post(ed, request, options);
        return response.data as GateCreatePostDocumentResponse;
    };

    gateRetrieveDocuments = async (
        gate: string,
        auditRecord: AuditRecord,
        actor: string = '',
    ) => {
        const request: GateRetrieveDocumentsRequest = {
            auditRecord: auditRecord,
            gateRetrieveDocumentsInputRecord: {
                gate: gate,
                source: null,
            },
        };
        if (actor != '') {
            request.gateRetrieveDocumentsInputRecord.source = {
                actor: {
                    id: actor,
                },
            };
        }
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        //post the request to the endpoint
        const ed = this.endpoint + '/gateRetrieveDocuments';
        const response = await axios.post(ed, request, options);
        return response.data as GateRetrieveDocumentsResponse;
    };
}
