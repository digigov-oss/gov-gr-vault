import fs from 'fs';
import crypto from 'crypto';
import {
    generateAuditRecord,
    AuditRecord,
    FileEngine,
    AuditEngine,
} from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';
import type {
    Statement,
    Attachment,
    Target,
    GateRetrieveDocumentsResponse,
    GateCreatePostDocumentResponse,
    KedResponseWithAuditRecord,
} from './interface';
export * from './interface';

/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};

export interface ApplicationFields {
    [key: string]: string | Uint8Array;
}

/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class Vault {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;

    constructor(user: string, pass: string, overrides?: Overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) {
            this.endpoint = overrides?.endpoint;
        } else {
            this.endpoint = this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? ({} as AuditRecord);
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }

    genAuditRecord = async (overrides?: Overrides): Promise<AuditRecord> => {
        const auditInit = Object.assign(
            {},
            this.auditInit,
            overrides?.auditInit,
        );
        const auditRecord = await generateAuditRecord(
            auditInit,
            this.auditEngine,
        );
        if (!auditRecord) throw new Error('Audit record is not initialized');
        return auditRecord;
    };

    /**
     * Expects a full path to a pdf file or a byte array.
     * @param pathorui8a path or byte array
     * @param key string provided from template
     * @returns Attachment;
     * @throws Error;
     * @description generates an attachment object from a pdf file
     * @example
     * const attachment = genBase64PDFAttachment("/tmp/test.pdf","test");
     */
    public genBase64PDFAttachment = (
        pathorui8a: string | Uint8Array,
        key: string,
    ): Attachment => {
        // check if pathorui8a is string or a Uint8Array
        if (
            typeof pathorui8a !== 'string' &&
            !(pathorui8a instanceof Uint8Array)
        )
            throw new Error('pathorui8a must be a string or a Uint8Array');
        let fileName = '';
        let contents = '';
        if (!(pathorui8a instanceof Uint8Array)) {
            const path = pathorui8a;
            if (!fs.existsSync(path))
                throw new Error('File does not exist on path: ' + path);
            if (path.split('.').pop() !== 'pdf')
                throw new Error('File does not have PDF extension');
            fileName = '' + path.split('/').pop();
            contents = fs.readFileSync(path, { encoding: 'base64' });
        } else {
            fileName = key + '.pdf';
            contents = Buffer.from(pathorui8a).toString('base64');
        }
        //generate digest sha256 of the contents
        const digest = crypto
            .createHash('sha256')
            .update(contents)
            .digest('base64');
        return {
            [key]: {
                filename: fileName,
                'content-encoding': 'base64',
                'digest-sha256': digest,
                content: contents,
            },
        };
    };

    public postDocument = async (
        statements: ApplicationFields,
        attachments: ApplicationFields,
        targets: Target[],
        template: string,
        gate: string,
        actor: string,
        isOfficial: boolean = true,
        export_document_pdf: boolean = false,
        overrides?: Overrides,
    ): Promise<KedResponseWithAuditRecord<GateCreatePostDocumentResponse>> => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        //for each attachment generate an attachment object
        let ats: Attachment = {};
        for (const key in attachments) {
            const attachment = this.genBase64PDFAttachment(
                attachments[key],
                key,
            );
            ats = { ...ats, ...attachment };
        }
        // and statements
        let sts: Statement = {};
        for (const key in statements) {
            sts[key] = String(statements[key]);
        }
        const resp = await restClient.gateCreatePostDocument(
            gate,
            template,
            sts,
            ats,
            targets,
            auditRecord,
            actor,
            isOfficial,
            export_document_pdf
        );
        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };

    public getDocuments = async (
        gate: string,
        actor: string = '',
        overrides?: Overrides,
    ): Promise<KedResponseWithAuditRecord<GateRetrieveDocumentsResponse>> => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const kedResponse = await restClient.gateRetrieveDocuments(
            gate,
            auditRecord,
            actor,
        );
        return {
            kedResponse: kedResponse,
            auditRecord: auditRecord,
        };
    };
}

export default Vault;
