import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';

/* Posible target references: */
declare type case_id = string;
declare type entry_id = string;
declare type gate_id = string;
export interface PersonIdentifier {
    afm: string;
    firstname: string;
    surname: string;
}

/*
 * Possible target combinations
 *
 * list of locations where the document is requested to be posted (after its publication/issued)
 *
 * type=case: Based on the case number.
 * Each document belongs to a case (case_id key). If the document being created is a response
 * for an existing case, a post is made in the inbox of those involved
 * in the case (eg a citizen who has submitted an application).
 *
 * type=entry: Based on post number. A document (eg an application) may be posted in a service's vault.
 * Based on ID of this post, the document being created will be posted to the vault of those involved in
 * the original document (in the vault of the citizen who has submit the application).
 *
 * type=person: To a citizen based on their VAT number and name.
 *
 * type=gate: In a box based on its code name (e.g. on the service vault).
 */
export declare type Target =
    | {
          type: 'case';
          ref: case_id;
      }
    | {
          type: 'entry';
          ref: entry_id;
      }
    | {
          type: 'person';
          ref: PersonIdentifier;
      }
    | {
          type: 'gate';
          ref: gate_id;
      };

/* Possible document states: */
/* requested,dismissed and deleted is undocumented but used for declaration state*/
export declare type State =
    | 'issued'
    | 'editing'
    | 'revoked'
    | 'requested'
    | 'dismissed'
    | 'deleted';

/* Possible attachment retrieval types:
 * "content-uri"(default): to provide a link to download the attached files
 * "content": to include base64 files in its structure document
 * "no-content": to not include the content of the files
 */
export declare type Retrieval = 'content-uri' | 'content' | 'no-content';

/*  Actor identified only by id! */
export declare type Actor = {
    id: string;
};

// Interface for the display
/*
 * Contains additional display information document.
 * Template may provide for this step:
 * Display values not included in statements and attachments:
 * <name>/value keys for some <name> key.
 * Display information and display order of options for fields that
 * accept values from a list of choices: <name>/choices-order is the order
 * display of choices and <name>/choices/<choice> is the literal
 * for the <choice> option.
 */
export interface Display {
    [key: string]: string;
}

//Statements are related to the selected template
export interface Statement {
    [key: string]: string;
}

export interface AttachmentBase {
    filename: string;
    'digest-sha256': '' | string; //sha256 of the attachment or empty string
    'content-encoding': 'base64'; //always base64
    'content-uri'?: string;
    content?: string;
}

export interface AttachmentWithContent extends AttachmentBase {
    content: string; //base64 encoded
    'content-uri'?: never;
}

export interface AttachmentWithURL extends AttachmentBase {
    'content-encoding': never;
    content?: never;
    'content-uri': string;
}

export interface AttachmentResponse {
    filename: string;
    'digest-sha256': string;
}

export declare type Attachment = {
    [key: string]:
        | AttachmentWithContent
        | AttachmentWithURL
        | AttachmentResponse;
};

export declare type Template = {
    refname: string; //name of the template, sould be provided by digigov
    'digest-sha256': string | null; //sha256 of the template, not yet used
};

export declare type Document = {
    statements: Statement;
    attachments: Attachment;
    template: Template;
    state: State;
    step?: string;
    'declaration-id'?: string; //The ID of the fill stream. Useful in case where a document is filled in gradually.
    case_id?: string | null; //The identifier of the case to which the document belongs.
    'document-id'?: string | null; //Generated. The ID of the issued document. Is cryptographically secure and used to verify the document. If a document has not been issued, the field has a null value.
    'document-title'?: string | null;
    timestamp?: string | null; //Generated. The timestamp of the document. If a document has not been issued, the field has a null value.
    is_official: boolean; // defines whether the document is official or tentative.
    issuer: string; //always "services.gov.gr"
    display?: Display;
    'digest-256'?: string | null; //Not used
    'document-refs'?: {
        //Generated
        [key: string]: Document; //key is the document id
    };
    [key: string]: any; //Document can have other custom fields that returned on response
};

export declare type GateCreatePostDocumentInputRecord = {
    gate: gate_id; //GOV.GR vault name (mandatory)
    source: {
        actor?: Actor;
        document: Document;
        targets: Target[];
        export_document_pdf?: boolean; //to include the pdf of the document in the response, if supported by the template.
        attachment_retrieval?: Retrieval;
    };
};

export declare type GateRetrieveDocumentsInputRecord = {
    gate: gate_id; //GOV.GR vault name (mandatory)
    source: {
        actor?: Actor;
    } | null;
};

export declare type GateRetrieveDocumentsRequest = {
    auditRecord: AuditRecord;
    gateRetrieveDocumentsInputRecord: GateRetrieveDocumentsInputRecord;
};

export declare type GateRetrieveDocumentsOutputRecord = {
    data: {
        id: string; //Generated. The ID of the document.
        gate_id: string; //GOV.GR vault name
        status: string; //The status of the document. Possible values: "incoming", ....
        document: Document;
    }[];
};
export declare type GateRetrieveDocumentsResponse = {
    gateRetrieveDocumentsOutputRecord: GateRetrieveDocumentsOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};

export declare type GateCreatePostDocumentRequest = {
    auditRecord: AuditRecord;
    gateCreatePostDocumentInputRecord: GateCreatePostDocumentInputRecord;
};

export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};

export declare type GateCreatePostDocumentOutputRecord = {
    data: {
        document: Document;
        'document-pdf'?: AttachmentWithContent;
    };
};

export declare type GateCreatePostDocumentResponse = {
    gateCreatePostDocumentOutputRecord?: GateCreatePostDocumentOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};

export interface KedResponseWithAuditRecord<T> {
    kedResponse: T;
    auditRecord: AuditRecord;
}
