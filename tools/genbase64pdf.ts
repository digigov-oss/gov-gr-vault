import pdfGen from './pdfgen';
import fs from 'fs';
const now = new Date().toISOString();
pdfGen(now);
// convert to base64
const base64 = fs.readFileSync('/tmp/test.pdf', 'base64');
//print to console
console.log(base64);
