import { getAppFields } from '../src/helpers';
const test = async (template: string, step: string = '') => {
    const APPLICATION_FIELDS = await getAppFields(template, step);
    console.log(APPLICATION_FIELDS);
};

//get cli input named variables "template" and "step"
const [, , template, step] = process.argv;
test(template, step);
