import Vault, { ApplicationFields } from '../src/index';
import config from './config.json';
import inspect from 'object-inspect';
import pdfGen from '../tools/pdfgen';

//You should take the following information from the provided template
const TEMPLATE = 'EUGO-FOREAS';
const GATE = 'EUGO-FOREAS';
const ACTOR = 'service/eugo-foreas';
const attachmentFields = ['aitisi', 'dikaiologitiko_1'];
const statementFields = ['description', 'comments'];

const test = async () => {
    //use class vault to create a new vault instance
    const vault = new Vault(config.user, config.pass, config.overrides);

    const now = new Date().toISOString();
    for (let i = 0; i < attachmentFields.length; i++) {
        pdfGen(
            attachmentFields[i] + ':' + now,
            '/tmp/' + attachmentFields[i] + '.pdf',
        );
    }

    let attachments: ApplicationFields = {};
    for (let i = 0; i < attachmentFields.length; i++) {
        attachments[attachmentFields[i]] =
            '/tmp/' + attachmentFields[i] + '.pdf';
    }

    attachments['dikaiologitiko_2'] = Uint8Array.from(
        pdfGen('dikaiologitiko_2 buffer:' + now)
            .split('')
            .map((x) => x.charCodeAt(0)),
    );

    let statements: ApplicationFields = {};
    for (let i = 0; i < statementFields.length; i++) {
        statements[statementFields[i]] = statementFields[i] + ':' + now;
    }
    const targets = [{ type: 'gate', ref: GATE }];

    console.log(
        inspect(
            await vault.postDocument(
                statements,
                attachments,
                targets as any,
                TEMPLATE,
                GATE,
                ACTOR,
                false,
            ),
            { depth: 10 },
        ),
    );
};

test();
