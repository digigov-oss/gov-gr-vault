import Vault from '../dist/esm/index.js';
import config from './config.json';
import inspect from 'object-inspect';

const simplepdf = "%PDF-1.4\n"+
"1 0 obj\n<< /Type /Catalog /Pages 2 0 R >>\nendobj\n"+
"2 0 obj\n<< /Type /Pages /Kids [ 3 0 R ] /Count 1 >>\nendobj\n"+
"3 0 obj\n<< /Type /Page /Parent 2 0 R /Resources << /Font << /F1 4 0 R >> >> /Contents 5 0 R >>\nendobj\n" +
"4 0 obj\n<< /Type /Font /Subtype /Type1 /BaseFont /Helvetica >>\nendobj\n" +
"5 0 obj\n<< /Length 6 0 R >>\nstream\nBT /F1 12 Tf 72 720 Td (Hello World) Tj ET\nendstream\nendobj\n" +
"6 0 obj\n20\nendobj\n" +
"xref\n0 7\n" +
"0000000000 65535 f \n" +
"0000000015 00000 n \n" +
"0000000071 00000 n \n" +
"0000000127 00000 n \n" +
"0000000193 00000 n \n" +
"0000000259 00000 n \n" +
"0000000279 00000 n \n" +
"trailer\n<< /Size 7 /Root 1 0 R >>\n" +
"startxref\n294\n%%EOF\n";

//You should take the following information from the provided template
const TEMPLATE = 'EUGO-FOREAS';
const GATE = 'EUGO-FOREAS';
const ACTOR = 'service/eugo-foreas';
const attachmentFields = ['aitisi', 'dikaiologitiko_1','dikaiologitiko_2'];
const statementFields = ['description', 'comments'];

const test = async () => {
    //use class vault to create a new vault instance
    const vault = new Vault(
        config.user,
        config.pass,
        TEMPLATE,
        GATE,
        ACTOR,
        config.overrides,
    );

    let attachments = {};
    for (let i = 0; i < attachmentFields.length; i++) {
        attachments[attachmentFields[i]] = Uint8Array.from(simplepdf.split('').map((x) => x.charCodeAt(0)));
    }

    let statements = {};
    const now = new Date().toISOString();
    for (let i = 0; i < statementFields.length; i++) {
        statements[statementFields[i]] = statementFields[i] + ':' + now;
    }
    const targets = [{ type: 'gate', ref: GATE }];
    
    console.log(
        inspect(
            await vault.postDocument(
                statements,
                attachments,
                targets,
                ACTOR,
            ),
            { depth: 10 },
        ),
    );
};

test();