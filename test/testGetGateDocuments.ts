import Vault from '../src/index';
import config from './config.json';
import inspect from 'object-inspect';

const GATE = 'EUGO-FOREAS';
const ACTOR = 'service/eugo-foreas';

const test = async () => {
    //use class vault to create a new vault instance
    const vault = new Vault(config.user, config.pass, config.overrides);
    console.log(inspect(await vault.getDocuments(GATE, ACTOR), { depth: 10 }));
};

test();
