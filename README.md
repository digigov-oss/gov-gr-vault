# Gov GR Vault

Module to consume gov.gr Vault service.

## Statements and attachments

Statements and attachment keys are different in each template. For example template -EUGO-FOREAS- has two
-statements- with `description` and `comments` keys and -attachments- with `dikaiologitiko_1`, `dikaiologitiko_2` and `aitisi` keys (aitisi is also a required key). Thus, using this module requires the user to provide both
the keys and the information needed in the corresponding template.

Example:

```
const statements:ApplicationFields = {
    description: "just a test",
    comments: "with comments"
}
const attachments:ApplicationFields = {
    aitisi:"/tmp/aitisi.pdf",
    dikaiologitiko_1:"/tmp/dikaiologitiko_1.pdf",
    dikaiologitiko_2:"/tmp/dikaiologitiko_2.pdf"
}
```

Attachment value can also be a bufer of Uint8Array with the contents of the pdf file. The contents are automatically
encoded to base64 inside the library.

## Targets

Targets are a list of locations where the document is requested to be posted (after its publication/issued) and can be of the following types:

-   type=case: Based on the case number
-   type=entry: Based on post number.
-   type=person: To a citizen based on their VAT number and name.
-   type=gate: In a gate based on its code name (e.g. on the service vault).

Please refer to the digigov documentation `govGateRestService` for more details.

Example:

```
const Targets = [
    {"type": "gate", "ref": "EUGO-FOREAS"}
]
```

# Tools

For convenience there is a tool to get application fields from a given published template. The tool uses the API
in order to help you create the statements and the attachments objects.

Use `yarn getAppFields EUGO-FOREAS` to get the fields of first step of EUGO-FOREAS template.
Use `yarn getAppFields EUGO-FOREAS final_view:entity` to get the fields of final_view:entity step of EUGO-FOREAS template.
Use `yarn getAppSteps EUGO-FOREAS` to get the available steps for a template.
Usually for the last step "final_view" you can append ":entity" or ":creator" even if not listed.
Finally, there is a tool to create a pdf file for tests.
Use `yarn genBase64PDF` to creates a pdf file with current date as text and get the base64 string.
