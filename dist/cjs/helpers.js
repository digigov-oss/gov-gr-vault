"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getTemplateInfo = exports.getTemplateSteps = exports.getAppFields = void 0;
const config_json_1 = __importDefault(require("./config.json"));
const axios_1 = __importDefault(require("axios"));
const getAppFields = (template, step = '') => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const url = `${config_json_1.default.dilosi_url}/api/templates/${template}/`;
        const response = yield axios_1.default.get(url);
        const steps = response.data.steps_spec['steps-order'];
        if (step === '') {
            step = String(steps).split(' ')[0];
        }
        const fields = response.data.steps_spec.steps[step].fields;
        let statements = {};
        let attachments = {};
        const APPLICATION_FIELDS = Object.keys(fields);
        for (let i = 0; i < APPLICATION_FIELDS.length; i++) {
            const field = APPLICATION_FIELDS[i];
            if (fields[field].datatype === 'attachment') {
                Object.assign(attachments, { [field]: '' });
            }
            else {
                Object.assign(statements, { [field]: '' });
            }
        }
        return { statements: statements, attachments: attachments };
    }
    catch (error) {
        console.log(error);
    }
});
exports.getAppFields = getAppFields;
const getTemplateSteps = (template) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const url = `${config_json_1.default.dilosi_url}/api/templates/${template}/`;
        const response = yield axios_1.default.get(url);
        const steps = response.data.steps_spec['steps-order'];
        return steps;
    }
    catch (error) {
        console.log(error);
    }
});
exports.getTemplateSteps = getTemplateSteps;
const getTemplateInfo = (template) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const url = `${config_json_1.default.dilosi_url}/api/templates/${template}/`;
        const response = yield axios_1.default.get(url);
        return response.data;
    }
    catch (error) {
        console.log(error);
    }
});
exports.getTemplateInfo = getTemplateInfo;
exports.default = exports.getTemplateInfo;
