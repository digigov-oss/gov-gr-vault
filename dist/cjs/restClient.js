"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RestClient = void 0;
const axios_1 = __importDefault(require("axios"));
class RestClient {
    constructor(user, pass, endpoint) {
        /**
         *
         * @param path string;
         * @returns
         */
        this.gateCreatePostDocument = (gate, template, statements, attachments, targets, auditRecord, actor = '', isOfficial = false, export_document_pdf = false) => __awaiter(this, void 0, void 0, function* () {
            const request = {
                auditRecord: auditRecord,
                gateCreatePostDocumentInputRecord: {
                    gate: gate,
                    source: {
                        document: {
                            statements: statements,
                            attachments: attachments,
                            issuer: 'services.gov.gr',
                            is_official: isOfficial,
                            template: {
                                refname: template,
                                'digest-sha256': null, //This does not used and is always null
                            },
                            timestamp: null,
                            'document-id': null,
                            state: 'issued',
                            'digest-sha256': '', //This does not used and is always empty string
                        },
                        targets: targets,
                        export_document_pdf: export_document_pdf
                    },
                },
            };
            if (actor != '') {
                request.gateCreatePostDocumentInputRecord.source.actor = {
                    id: actor,
                };
            }
            const options = {
                auth: {
                    username: this.user,
                    password: this.pass,
                },
            };
            // post the request to the endpoint
            const ed = this.endpoint + '/gateCreatePostDocument';
            const response = yield axios_1.default.post(ed, request, options);
            return response.data;
        });
        this.gateRetrieveDocuments = (gate, auditRecord, actor = '') => __awaiter(this, void 0, void 0, function* () {
            const request = {
                auditRecord: auditRecord,
                gateRetrieveDocumentsInputRecord: {
                    gate: gate,
                    source: null,
                },
            };
            if (actor != '') {
                request.gateRetrieveDocumentsInputRecord.source = {
                    actor: {
                        id: actor,
                    },
                };
            }
            const options = {
                auth: {
                    username: this.user,
                    password: this.pass,
                },
            };
            //post the request to the endpoint
            const ed = this.endpoint + '/gateRetrieveDocuments';
            const response = yield axios_1.default.post(ed, request, options);
            return response.data;
        });
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }
}
exports.RestClient = RestClient;
