import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
declare type case_id = string;
declare type entry_id = string;
declare type gate_id = string;
export interface PersonIdentifier {
    afm: string;
    firstname: string;
    surname: string;
}
export declare type Target = {
    type: 'case';
    ref: case_id;
} | {
    type: 'entry';
    ref: entry_id;
} | {
    type: 'person';
    ref: PersonIdentifier;
} | {
    type: 'gate';
    ref: gate_id;
};
export declare type State = 'issued' | 'editing' | 'revoked' | 'requested' | 'dismissed' | 'deleted';
export declare type Retrieval = 'content-uri' | 'content' | 'no-content';
export declare type Actor = {
    id: string;
};
export interface Display {
    [key: string]: string;
}
export interface Statement {
    [key: string]: string;
}
export interface AttachmentBase {
    filename: string;
    'digest-sha256': '' | string;
    'content-encoding': 'base64';
    'content-uri'?: string;
    content?: string;
}
export interface AttachmentWithContent extends AttachmentBase {
    content: string;
    'content-uri'?: never;
}
export interface AttachmentWithURL extends AttachmentBase {
    'content-encoding': never;
    content?: never;
    'content-uri': string;
}
export interface AttachmentResponse {
    filename: string;
    'digest-sha256': string;
}
export declare type Attachment = {
    [key: string]: AttachmentWithContent | AttachmentWithURL | AttachmentResponse;
};
export declare type Template = {
    refname: string;
    'digest-sha256': string | null;
};
export declare type Document = {
    statements: Statement;
    attachments: Attachment;
    template: Template;
    state: State;
    step?: string;
    'declaration-id'?: string;
    case_id?: string | null;
    'document-id'?: string | null;
    'document-title'?: string | null;
    timestamp?: string | null;
    is_official: boolean;
    issuer: string;
    display?: Display;
    'digest-256'?: string | null;
    'document-refs'?: {
        [key: string]: Document;
    };
    [key: string]: any;
};
export declare type GateCreatePostDocumentInputRecord = {
    gate: gate_id;
    source: {
        actor?: Actor;
        document: Document;
        targets: Target[];
        export_document_pdf?: boolean;
        attachment_retrieval?: Retrieval;
    };
};
export declare type GateRetrieveDocumentsInputRecord = {
    gate: gate_id;
    source: {
        actor?: Actor;
    } | null;
};
export declare type GateRetrieveDocumentsRequest = {
    auditRecord: AuditRecord;
    gateRetrieveDocumentsInputRecord: GateRetrieveDocumentsInputRecord;
};
export declare type GateRetrieveDocumentsOutputRecord = {
    data: {
        id: string;
        gate_id: string;
        status: string;
        document: Document;
    }[];
};
export declare type GateRetrieveDocumentsResponse = {
    gateRetrieveDocumentsOutputRecord: GateRetrieveDocumentsOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
export declare type GateCreatePostDocumentRequest = {
    auditRecord: AuditRecord;
    gateCreatePostDocumentInputRecord: GateCreatePostDocumentInputRecord;
};
export declare type ErrorRecord = {
    errorCode: string;
    errorDescr: string;
};
export declare type GateCreatePostDocumentOutputRecord = {
    data: {
        document: Document;
        'document-pdf'?: AttachmentWithContent;
    };
};
export declare type GateCreatePostDocumentResponse = {
    gateCreatePostDocumentOutputRecord?: GateCreatePostDocumentOutputRecord;
    callSequenceId: string;
    callSequenceDate: string;
    errorRecord?: ErrorRecord;
};
export interface KedResponseWithAuditRecord<T> {
    kedResponse: T;
    auditRecord: AuditRecord;
}
export {};
