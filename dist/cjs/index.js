"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const crypto_1 = __importDefault(require("crypto"));
const gsis_audit_record_db_1 = require("@digigov-oss/gsis-audit-record-db");
const restClient_js_1 = require("./restClient.js");
const config_json_1 = __importDefault(require("./config.json"));
__exportStar(require("./interface"), exports);
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class Vault {
    constructor(user, pass, overrides) {
        var _a, _b, _c, _d;
        this.genAuditRecord = (overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditInit = Object.assign({}, this.auditInit, overrides === null || overrides === void 0 ? void 0 : overrides.auditInit);
            const auditRecord = yield (0, gsis_audit_record_db_1.generateAuditRecord)(auditInit, this.auditEngine);
            if (!auditRecord)
                throw new Error('Audit record is not initialized');
            return auditRecord;
        });
        /**
         * Expects a full path to a pdf file or a byte array.
         * @param pathorui8a path or byte array
         * @param key string provided from template
         * @returns Attachment;
         * @throws Error;
         * @description generates an attachment object from a pdf file
         * @example
         * const attachment = genBase64PDFAttachment("/tmp/test.pdf","test");
         */
        this.genBase64PDFAttachment = (pathorui8a, key) => {
            // check if pathorui8a is string or a Uint8Array
            if (typeof pathorui8a !== 'string' &&
                !(pathorui8a instanceof Uint8Array))
                throw new Error('pathorui8a must be a string or a Uint8Array');
            let fileName = '';
            let contents = '';
            if (!(pathorui8a instanceof Uint8Array)) {
                const path = pathorui8a;
                if (!fs_1.default.existsSync(path))
                    throw new Error('File does not exist on path: ' + path);
                if (path.split('.').pop() !== 'pdf')
                    throw new Error('File does not have PDF extension');
                fileName = '' + path.split('/').pop();
                contents = fs_1.default.readFileSync(path, { encoding: 'base64' });
            }
            else {
                fileName = key + '.pdf';
                contents = Buffer.from(pathorui8a).toString('base64');
            }
            //generate digest sha256 of the contents
            const digest = crypto_1.default
                .createHash('sha256')
                .update(contents)
                .digest('base64');
            return {
                [key]: {
                    filename: fileName,
                    'content-encoding': 'base64',
                    'digest-sha256': digest,
                    content: contents,
                },
            };
        };
        this.postDocument = (statements, attachments, targets, template, gate, actor, isOfficial = true, export_document_pdf = false, overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditRecord = yield this.genAuditRecord(overrides);
            const restClient = new restClient_js_1.RestClient(this.user, this.pass, this.endpoint);
            //for each attachment generate an attachment object
            let ats = {};
            for (const key in attachments) {
                const attachment = this.genBase64PDFAttachment(attachments[key], key);
                ats = Object.assign(Object.assign({}, ats), attachment);
            }
            // and statements
            let sts = {};
            for (const key in statements) {
                sts[key] = String(statements[key]);
            }
            const resp = yield restClient.gateCreatePostDocument(gate, template, sts, ats, targets, auditRecord, actor, isOfficial, export_document_pdf);
            return {
                kedResponse: resp,
                auditRecord: auditRecord,
            };
        });
        this.getDocuments = (gate, actor = '', overrides) => __awaiter(this, void 0, void 0, function* () {
            const auditRecord = yield this.genAuditRecord(overrides);
            const restClient = new restClient_js_1.RestClient(this.user, this.pass, this.endpoint);
            const kedResponse = yield restClient.gateRetrieveDocuments(gate, auditRecord, actor);
            return {
                kedResponse: kedResponse,
                auditRecord: auditRecord,
            };
        });
        this.prod = (_a = overrides === null || overrides === void 0 ? void 0 : overrides.prod) !== null && _a !== void 0 ? _a : false;
        if (overrides === null || overrides === void 0 ? void 0 : overrides.endpoint) {
            this.endpoint = overrides === null || overrides === void 0 ? void 0 : overrides.endpoint;
        }
        else {
            this.endpoint = this.prod ? config_json_1.default.prod : config_json_1.default.test;
        }
        this.auditInit = (_b = overrides === null || overrides === void 0 ? void 0 : overrides.auditInit) !== null && _b !== void 0 ? _b : {};
        this.auditStoragePath = (_c = overrides === null || overrides === void 0 ? void 0 : overrides.auditStoragePath) !== null && _c !== void 0 ? _c : '/tmp';
        this.auditEngine =
            (_d = overrides === null || overrides === void 0 ? void 0 : overrides.auditEngine) !== null && _d !== void 0 ? _d : new gsis_audit_record_db_1.FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }
}
exports.default = Vault;
