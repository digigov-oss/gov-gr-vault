import { AuditRecord, AuditEngine } from '@digigov-oss/gsis-audit-record-db';
import type { Attachment, Target, GateRetrieveDocumentsResponse, GateCreatePostDocumentResponse, KedResponseWithAuditRecord } from './interface';
export * from './interface';
/**
 * @type Overrides
 * @description Overrides for the REST client
 * @param {string} endpoint - Endpoint to be used for the REST client
 * @param {boolean} prod - Set to true for production environment
 * @param {string} auditInit - Audit record initializer to be used for the audit record produced
 * @param {string} auditStoragePath - Path to the audit record storage
 * @param {AuditEngine} auditEngine - Audit engine to be used for the audit record produced
 */
export declare type Overrides = {
    endpoint?: string;
    prod?: boolean;
    auditInit?: AuditRecord;
    auditStoragePath?: string;
    auditEngine?: AuditEngine;
};
export interface ApplicationFields {
    [key: string]: string | Uint8Array;
}
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
declare class Vault {
    endpoint: string;
    prod: boolean;
    auditInit: object;
    auditStoragePath: string;
    auditEngine: AuditEngine;
    user: string;
    pass: string;
    constructor(user: string, pass: string, overrides?: Overrides);
    genAuditRecord: (overrides?: Overrides) => Promise<AuditRecord>;
    /**
     * Expects a full path to a pdf file or a byte array.
     * @param pathorui8a path or byte array
     * @param key string provided from template
     * @returns Attachment;
     * @throws Error;
     * @description generates an attachment object from a pdf file
     * @example
     * const attachment = genBase64PDFAttachment("/tmp/test.pdf","test");
     */
    genBase64PDFAttachment: (pathorui8a: string | Uint8Array, key: string) => Attachment;
    postDocument: (statements: ApplicationFields, attachments: ApplicationFields, targets: Target[], template: string, gate: string, actor: string, isOfficial?: boolean, export_document_pdf?: boolean, overrides?: Overrides) => Promise<KedResponseWithAuditRecord<GateCreatePostDocumentResponse>>;
    getDocuments: (gate: string, actor?: string, overrides?: Overrides) => Promise<KedResponseWithAuditRecord<GateRetrieveDocumentsResponse>>;
}
export default Vault;
