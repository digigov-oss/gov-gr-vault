import axios from 'axios';
export class RestClient {
    user;
    pass;
    endpoint;
    constructor(user, pass, endpoint) {
        this.user = user;
        this.pass = pass;
        this.endpoint = endpoint;
    }
    /**
     *
     * @param path string;
     * @returns
     */
    gateCreatePostDocument = async (gate, template, statements, attachments, targets, auditRecord, actor = '', isOfficial = false, export_document_pdf = false) => {
        const request = {
            auditRecord: auditRecord,
            gateCreatePostDocumentInputRecord: {
                gate: gate,
                source: {
                    document: {
                        statements: statements,
                        attachments: attachments,
                        issuer: 'services.gov.gr',
                        is_official: isOfficial,
                        template: {
                            refname: template,
                            'digest-sha256': null, //This does not used and is always null
                        },
                        timestamp: null,
                        'document-id': null,
                        state: 'issued',
                        'digest-sha256': '', //This does not used and is always empty string
                    },
                    targets: targets,
                    export_document_pdf: export_document_pdf
                },
            },
        };
        if (actor != '') {
            request.gateCreatePostDocumentInputRecord.source.actor = {
                id: actor,
            };
        }
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        // post the request to the endpoint
        const ed = this.endpoint + '/gateCreatePostDocument';
        const response = await axios.post(ed, request, options);
        return response.data;
    };
    gateRetrieveDocuments = async (gate, auditRecord, actor = '') => {
        const request = {
            auditRecord: auditRecord,
            gateRetrieveDocumentsInputRecord: {
                gate: gate,
                source: null,
            },
        };
        if (actor != '') {
            request.gateRetrieveDocumentsInputRecord.source = {
                actor: {
                    id: actor,
                },
            };
        }
        const options = {
            auth: {
                username: this.user,
                password: this.pass,
            },
        };
        //post the request to the endpoint
        const ed = this.endpoint + '/gateRetrieveDocuments';
        const response = await axios.post(ed, request, options);
        return response.data;
    };
}
