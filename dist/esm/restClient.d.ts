import { AuditRecord } from '@digigov-oss/gsis-audit-record-db';
import { Attachment, GateCreatePostDocumentResponse, Statement, Target, GateRetrieveDocumentsResponse } from './interface';
export declare class RestClient {
    user: string;
    pass: string;
    endpoint: string;
    constructor(user: string, pass: string, endpoint: string);
    /**
     *
     * @param path string;
     * @returns
     */
    gateCreatePostDocument: (gate: string, template: string, statements: Statement, attachments: Attachment, targets: Target[], auditRecord: AuditRecord, actor?: string, isOfficial?: boolean, export_document_pdf?: boolean) => Promise<GateCreatePostDocumentResponse>;
    gateRetrieveDocuments: (gate: string, auditRecord: AuditRecord, actor?: string) => Promise<GateRetrieveDocumentsResponse>;
}
