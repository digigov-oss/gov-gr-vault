import fs from 'fs';
import crypto from 'crypto';
import { generateAuditRecord, FileEngine, } from '@digigov-oss/gsis-audit-record-db';
import { RestClient } from './restClient.js';
import config from './config.json';
export * from './interface';
/**
 *
 * @param user string;
 * @param pass string;
 * @param overrides overrides;
 */
class Vault {
    endpoint;
    prod;
    auditInit;
    auditStoragePath;
    auditEngine;
    user;
    pass;
    constructor(user, pass, overrides) {
        this.prod = overrides?.prod ?? false;
        if (overrides?.endpoint) {
            this.endpoint = overrides?.endpoint;
        }
        else {
            this.endpoint = this.prod ? config.prod : config.test;
        }
        this.auditInit = overrides?.auditInit ?? {};
        this.auditStoragePath = overrides?.auditStoragePath ?? '/tmp';
        this.auditEngine =
            overrides?.auditEngine ?? new FileEngine(this.auditStoragePath);
        this.user = user;
        this.pass = pass;
    }
    genAuditRecord = async (overrides) => {
        const auditInit = Object.assign({}, this.auditInit, overrides?.auditInit);
        const auditRecord = await generateAuditRecord(auditInit, this.auditEngine);
        if (!auditRecord)
            throw new Error('Audit record is not initialized');
        return auditRecord;
    };
    /**
     * Expects a full path to a pdf file or a byte array.
     * @param pathorui8a path or byte array
     * @param key string provided from template
     * @returns Attachment;
     * @throws Error;
     * @description generates an attachment object from a pdf file
     * @example
     * const attachment = genBase64PDFAttachment("/tmp/test.pdf","test");
     */
    genBase64PDFAttachment = (pathorui8a, key) => {
        // check if pathorui8a is string or a Uint8Array
        if (typeof pathorui8a !== 'string' &&
            !(pathorui8a instanceof Uint8Array))
            throw new Error('pathorui8a must be a string or a Uint8Array');
        let fileName = '';
        let contents = '';
        if (!(pathorui8a instanceof Uint8Array)) {
            const path = pathorui8a;
            if (!fs.existsSync(path))
                throw new Error('File does not exist on path: ' + path);
            if (path.split('.').pop() !== 'pdf')
                throw new Error('File does not have PDF extension');
            fileName = '' + path.split('/').pop();
            contents = fs.readFileSync(path, { encoding: 'base64' });
        }
        else {
            fileName = key + '.pdf';
            contents = Buffer.from(pathorui8a).toString('base64');
        }
        //generate digest sha256 of the contents
        const digest = crypto
            .createHash('sha256')
            .update(contents)
            .digest('base64');
        return {
            [key]: {
                filename: fileName,
                'content-encoding': 'base64',
                'digest-sha256': digest,
                content: contents,
            },
        };
    };
    postDocument = async (statements, attachments, targets, template, gate, actor, isOfficial = true, export_document_pdf = false, overrides) => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        //for each attachment generate an attachment object
        let ats = {};
        for (const key in attachments) {
            const attachment = this.genBase64PDFAttachment(attachments[key], key);
            ats = { ...ats, ...attachment };
        }
        // and statements
        let sts = {};
        for (const key in statements) {
            sts[key] = String(statements[key]);
        }
        const resp = await restClient.gateCreatePostDocument(gate, template, sts, ats, targets, auditRecord, actor, isOfficial, export_document_pdf);
        return {
            kedResponse: resp,
            auditRecord: auditRecord,
        };
    };
    getDocuments = async (gate, actor = '', overrides) => {
        const auditRecord = await this.genAuditRecord(overrides);
        const restClient = new RestClient(this.user, this.pass, this.endpoint);
        const kedResponse = await restClient.gateRetrieveDocuments(gate, auditRecord, actor);
        return {
            kedResponse: kedResponse,
            auditRecord: auditRecord,
        };
    };
}
export default Vault;
