export declare const getAppFields: (template: string, step?: string) => Promise<{
    statements: {};
    attachments: {};
} | undefined>;
export declare const getTemplateSteps: (template: string) => Promise<string | undefined>;
export declare const getTemplateInfo: (template: string) => Promise<any>;
export default getTemplateInfo;
