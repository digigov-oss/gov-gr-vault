import config from './config.json';
import axios from 'axios';
export const getAppFields = async (template, step = '') => {
    try {
        const url = `${config.dilosi_url}/api/templates/${template}/`;
        const response = await axios.get(url);
        const steps = response.data.steps_spec['steps-order'];
        if (step === '') {
            step = String(steps).split(' ')[0];
        }
        const fields = response.data.steps_spec.steps[step].fields;
        let statements = {};
        let attachments = {};
        const APPLICATION_FIELDS = Object.keys(fields);
        for (let i = 0; i < APPLICATION_FIELDS.length; i++) {
            const field = APPLICATION_FIELDS[i];
            if (fields[field].datatype === 'attachment') {
                Object.assign(attachments, { [field]: '' });
            }
            else {
                Object.assign(statements, { [field]: '' });
            }
        }
        return { statements: statements, attachments: attachments };
    }
    catch (error) {
        console.log(error);
    }
};
export const getTemplateSteps = async (template) => {
    try {
        const url = `${config.dilosi_url}/api/templates/${template}/`;
        const response = await axios.get(url);
        const steps = response.data.steps_spec['steps-order'];
        return steps;
    }
    catch (error) {
        console.log(error);
    }
};
export const getTemplateInfo = async (template) => {
    try {
        const url = `${config.dilosi_url}/api/templates/${template}/`;
        const response = await axios.get(url);
        return response.data;
    }
    catch (error) {
        console.log(error);
    }
};
export default getTemplateInfo;
